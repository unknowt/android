package unknow.repository.remote.api

import androidx.lifecycle.LiveData
import retrofit2.http.Body
import retrofit2.http.POST
import unknow.common.apiconfig.Api.ApiResponse
import unknow.repository.remote.dto.*

interface IStore {
    @POST("/api/store/getliststore")
    fun getList(@Body request: GetStoreRequest): LiveData<ApiResponse<BaseResponse<MutableList<StoreDTO>>>>

    @POST("/api/store/getstore")
    fun getInfo(@Body model: GetStoreRequest): LiveData<ApiResponse<BaseResponse<StoreDTO>>>

    @POST("/api/store/savestore")
    fun createUpdate(@Body request: CreateUpdateStoreRequest): LiveData<ApiResponse<BaseResponse<BaseModel>>>

    @POST("/api/store/addhistory")
    fun logHistory(@Body request: LogHistoryRequest): LiveData<ApiResponse<BaseResponse<BaseModel>>>
}