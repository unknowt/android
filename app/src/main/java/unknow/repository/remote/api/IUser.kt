package unknow.repository.remote.api

import androidx.lifecycle.LiveData
import retrofit2.http.Body
import retrofit2.http.POST
import unknow.common.apiconfig.Api.ApiResponse
import unknow.repository.remote.dto.*

interface IUser {

    @POST("/api/user/login")
    fun login(@Body request: LoginRegisterRequest): LiveData<ApiResponse<BaseResponse<UserDTO>>>

    @POST("/api/user/changepersonalinfo")
    fun changeInfo(@Body request: ChangeInfoRequest): LiveData<ApiResponse<BaseResponse<BaseModel>>>
}