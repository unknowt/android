package unknow.repository.remote.api

import androidx.lifecycle.LiveData
import retrofit2.http.Body
import retrofit2.http.POST
import unknow.common.apiconfig.Api.ApiResponse
import unknow.repository.remote.dto.BaseResponse
import unknow.repository.remote.dto.UpImageRequest
import unknow.repository.remote.dto.UpImageResponse

interface IFile {
    @POST("/api/file/uploadfile")
    fun upImage(@Body request: UpImageRequest): LiveData<ApiResponse<BaseResponse<UpImageResponse>>>
}