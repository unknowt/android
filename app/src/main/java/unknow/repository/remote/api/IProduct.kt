package unknow.repository.remote.api

import androidx.lifecycle.LiveData
import retrofit2.http.Body
import retrofit2.http.POST
import unknow.common.apiconfig.Api.ApiResponse
import unknow.repository.remote.dto.BaseModel
import unknow.repository.remote.dto.BaseResponse
import unknow.repository.remote.dto.ProductDTO

interface IProduct {
    @POST("/api/product/getAll")
    fun getList(@Body model: BaseModel): LiveData<ApiResponse<BaseResponse<MutableList<ProductDTO>>>>

    @POST("/api/product/getActive")
    fun getActive(@Body model: BaseModel): LiveData<ApiResponse<BaseResponse<MutableList<ProductDTO>>>>

    @POST("/api/product/create")
    fun create(@Body request: ProductDTO): LiveData<ApiResponse<BaseResponse<BaseModel>>>

    @POST("/api/product/delete")
    fun delete(@Body model: BaseModel): LiveData<ApiResponse<BaseResponse<String>>>

    @POST("/api/product/update")
    fun update(@Body request: ProductDTO): LiveData<ApiResponse<BaseResponse<String>>>
}