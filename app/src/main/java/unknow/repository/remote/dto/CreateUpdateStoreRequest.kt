package unknow.repository.remote.dto

class CreateUpdateStoreRequest {
    var id: Int? = null
    var userID: Int? = null
    var name: String? = null
    var status: Int? = null
    var imageurl: String? = null
}