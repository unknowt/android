package unknow.repository.remote.dto

class BaseResponse<T> {
    var message: String? = null
    var status: Boolean? = null
    var data: T? = null
}