package unknow.repository.remote.dto

class UpImageRequest {
    var base64image: String? = null
    var filename: String? = null
}

class UpImageResponse {
    var uid: String? = null
}