package unknow.repository.remote.dto

class ChangeInfoRequest {
    var id: Int? = null
    var fullName: String? = null
    var avatar: String? = null
}