package unknow.repository.remote.dto

class ProductDTO {
    var id: Int? = null
    var storeid: Int? = null
    var name: String? = null
    var description: String? = null
    var price: Double? = null
    var imageurl: String? = null
    var status: Int? = null
    var usercreated: Int? = null
}