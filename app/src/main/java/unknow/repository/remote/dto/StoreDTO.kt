package unknow.repository.remote.dto

class StoreDTO {
    var id: Int? = null
    var userid: String? = null
    var name: String? = null
    var description: String? = null
    var address: String? = null
    var contact: String? = null
    var priority: Int? = null
    var imageurl: String? = null
    var status: Int? = null
    var location: String? = null
}