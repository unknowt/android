package unknow.repository.remote.dto

class UserDTO {
    var id: Int? = null
    var username: String? = null
    var password: String? = null
    var fullname: String? = null
    var avatar: String? = null
    var status: String? = null
    var isinsert: String? = null
}