package unknow.chopchep.home

import androidx.lifecycle.LiveData
import unknow.chopchep.base.BaseViewModel
import unknow.common.apiconfig.APIManager
import unknow.common.apiconfig.Api.ApiResponse
import unknow.common.apiconfig.Api.AppExecutors
import unknow.common.apiconfig.Api.NetworkBoundResource
import unknow.common.apiconfig.Api.Resource
import unknow.repository.remote.api.IStore
import unknow.repository.remote.dto.BaseResponse
import unknow.repository.remote.dto.GetStoreRequest
import unknow.repository.remote.dto.StoreDTO

class HomeViewModel : BaseViewModel() {
    val api by lazy {
        APIManager.client?.create(IStore::class.java)
    }

    fun getStores(appExecutors: AppExecutors): LiveData<Resource<BaseResponse<MutableList<StoreDTO>>>>? {
        return object : NetworkBoundResource<BaseResponse<MutableList<StoreDTO>>,BaseResponse<MutableList<StoreDTO>>>(appExecutors){
            override fun saveCallResult(item: BaseResponse<MutableList<StoreDTO>>) {
            }

            override fun shouldFetch(data: BaseResponse<MutableList<StoreDTO>>?): Boolean {
                return true
            }

            override fun shouldSave(): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<BaseResponse<MutableList<StoreDTO>>>? {
                return null
            }

            override fun createCall(): LiveData<ApiResponse<BaseResponse<MutableList<StoreDTO>>>> {
                return api?.getList(GetStoreRequest())!!
            }

        }.asLiveData()
    }
}