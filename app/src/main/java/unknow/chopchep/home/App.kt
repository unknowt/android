package unknow.chopchep.home

import android.app.Application
import android.content.Context
import unknow.chopchep.R
import unknow.chopchep.base.Base
import unknow.common.apiconfig.APIManager

class App : Application() {

    //    companion object {
//        var db: AppDB? = null
//    }
//
    override fun onCreate() {
        super.onCreate()
//        db = AppDB.newInstance(applicationContext)
        Base.sharedPreferences =  getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE)
        APIManager.url = "http://103.63.215.41:69"
        APIManager.imgBaseURL = "http://103.63.215.41:69/api/file/getfile?uid="
    }
}