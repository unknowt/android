package unknow.chopchep.home

import android.app.Activity
import android.content.Intent
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.nav_customer_info.view.*
import kotlinx.android.synthetic.main.nav_login.view.*
import kotlinx.android.synthetic.main.rcv_store_row.view.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.startActivityForResult
import unknow.chopchep.R
import unknow.chopchep.base.Base
import unknow.chopchep.base.BaseActivity
import unknow.chopchep.login.LoginActivity
import unknow.chopchep.product.ProductActivity
import unknow.chopchep.storeinfo.StoreInfoActivity
import unknow.chopchep.userinfo.UserInfoActivity
import unknow.common.apiconfig.APIManager
import unknow.common.customcontrol.init
import unknow.common.customcontrol.reloadData
import unknow.common.extension.*
import unknow.repository.remote.dto.StoreDTO


class HomeActivity : BaseActivity() {

    private val vm by lazy {
        ViewModelProviders.of(this).get(HomeViewModel::class.java)
    }

    override var layoutRes = R.layout.activity_home

    private var lsData = mutableListOf<StoreDTO>()
    private var reloadCode = 888

    override fun getData() {
        super.getData()
        getStores()
    }

    private fun getStores() {
        vm.getStores(appExecutors)?.observe(this, Observer {
            it.handle({
                lsData = it.data ?: mutableListOf()
                rcv_store.init(
                    lsData,
                    R.layout.rcv_store_row,
                    GridLayoutManager(this, 2)
                ) { view, data ->
                    view.itemView.apply {
                        tv_store_name.text = data.name

                        imv.loadImage(APIManager.imgBaseURL + data.imageurl, R.drawable.ic_store_default)
                        setOnClickListener {
                            startActivity<ProductActivity>(getString(R.string.store) to data.jsonString)
                        }
                    }
                }
            })
        })
    }

    override fun configUI() {
        super.configUI()
        changeNav()
    }

    private fun changeNav() {
        val infoView = fl_content.findViewWithTag<View>("Info_View")
            ?: View.inflate(this, R.layout.nav_customer_info, null).apply {
                tag = "Info_View"
        }

        val loginView = fl_content.findViewWithTag<View>("Login_View")
            ?: View.inflate(this, R.layout.nav_login, null).apply {
                tag = "Login_View"
            }

        Base.userDTO?.let {
            fl_content.removeView(loginView)
            fl_content.addView(infoView)
            navHeaderClick()
            return
        }

        fl_content.removeView(infoView)
        fl_content.addView(loginView)
        navHeaderClick()
    }

    private fun navHeaderClick() {
        fl_content.btn_login?.setOnClickListener {
            startActivity<LoginActivity>()
            drawer_layout.closeDrawer(GravityCompat.START)
        }

        fl_content.tv_user_info?.setOnClickListener {
            startActivity<UserInfoActivity>()
            drawer_layout.closeDrawer(GravityCompat.START)
        }

        fl_content.tv_store_info?.setOnClickListener {
            startActivityForResult<StoreInfoActivity>(reloadCode)
            drawer_layout.closeDrawer(GravityCompat.START)
        }

        fl_content.btn_logout?.setOnClickListener {
            Base.userDTO = null
            drawer_layout.closeDrawer(GravityCompat.START)
            changeNav()
        }
    }

    override fun eventHandlers() {
        super.eventHandlers()
        navHeaderClick()

        btn_menu.setOnClickListener {
            drawer_layout.openDrawer(GravityCompat.START)
            Base.userDTO?.let { user ->
                fl_content?.tv_user_name?.text = user.fullname ?: user.username
                fl_content?.iv_avatar?.loadCircleImage(APIManager.imgBaseURL + user.avatar, R.drawable.ic_user_default)
            }
        }

        btn_search.setOnClickListener {
            cl_search.visiable
            edt_search.requestFocus()
            edt_search.showKeyBoard()
        }

        btn_cancel.setOnClickListener {
            cl_search.gone
            edt_search.setText("")
            edt_search.hideKeyBoard()
            rcv_store.reloadData(lsData)
        }

        edt_search.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                edt_search.hideKeyBoard()
                val lsSearch = lsData.filter { it.name?.contains(v.text) == true }
                rcv_store.reloadData(lsSearch.toMutableList())
            }
            true
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == reloadCode && resultCode == Activity.RESULT_OK){
            getStores()
        }
    }
}