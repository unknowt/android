package unknow.chopchep.storeinfo

import androidx.lifecycle.LiveData
import unknow.chopchep.base.BaseViewModel
import unknow.common.apiconfig.APIManager
import unknow.common.apiconfig.Api.ApiResponse
import unknow.common.apiconfig.Api.AppExecutors
import unknow.common.apiconfig.Api.NetworkBoundResource
import unknow.common.apiconfig.Api.Resource
import unknow.repository.remote.api.IProduct
import unknow.repository.remote.api.IStore
import unknow.repository.remote.dto.*

class StoreInfoViewModel: BaseViewModel(){

    private val apiStore by lazy {
        APIManager.client?.create(IStore::class.java)
    }

    private val apiProduct by lazy {
        APIManager.client?.create(IProduct::class.java)
    }

    var store: StoreDTO? = null

    fun getInfo(
        appExecutors: AppExecutors,
        request: GetStoreRequest
    ): LiveData<Resource<BaseResponse<StoreDTO>>> {
        return object : NetworkBoundResource<BaseResponse<StoreDTO>, BaseResponse<StoreDTO>>(appExecutors){
            override fun saveCallResult(item: BaseResponse<StoreDTO>) {

            }

            override fun shouldFetch(data: BaseResponse<StoreDTO>?): Boolean {
                return true
            }

            override fun shouldSave(): Boolean {
                return false
            }
            override fun loadFromDb(): LiveData<BaseResponse<StoreDTO>>? {
                return null
            }

            override fun createCall(): LiveData<ApiResponse<BaseResponse<StoreDTO>>> {
                return apiStore?.getInfo(request)!!
            }
        }.asLiveData()
    }

    fun getAllItemsOfStore(
        appExecutors: AppExecutors,
        baseModel: BaseModel
    ): LiveData<Resource<BaseResponse<MutableList<ProductDTO>>>> {
        return object :
            NetworkBoundResource<BaseResponse<MutableList<ProductDTO>>, BaseResponse<MutableList<ProductDTO>>>(
                appExecutors
            ) {
            override fun saveCallResult(item: BaseResponse<MutableList<ProductDTO>>) {

            }

            override fun shouldFetch(data: BaseResponse<MutableList<ProductDTO>>?): Boolean {
                return true
            }

            override fun shouldSave(): Boolean {
                return false
            }

            override fun loadFromDb(): LiveData<BaseResponse<MutableList<ProductDTO>>>? {
                return null
            }

            override fun createCall(): LiveData<ApiResponse<BaseResponse<MutableList<ProductDTO>>>> {
                return apiProduct?.getList(baseModel)!!
            }

        }.asLiveData()
    }

    fun updateInfo(
        appExecutors: AppExecutors,
        request: CreateUpdateStoreRequest
    ): LiveData<Resource<BaseResponse<BaseModel>>> {
        return object : NetworkBoundResource<BaseResponse<BaseModel>, BaseResponse<BaseModel>>(appExecutors){
            override fun saveCallResult(item: BaseResponse<BaseModel>) {

            }

            override fun shouldFetch(data: BaseResponse<BaseModel>?): Boolean {
                return true
            }

            override fun shouldSave(): Boolean {
                return false
            }
            override fun loadFromDb(): LiveData<BaseResponse<BaseModel>>? {
                return null
            }

            override fun createCall(): LiveData<ApiResponse<BaseResponse<BaseModel>>> {
                return apiStore?.createUpdate(request)!!
            }
        }.asLiveData()
    }
}