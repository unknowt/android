package unknow.chopchep.storeinfo

import android.content.Intent
import android.graphics.Color
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.activity_store_info.*
import unknow.chopchep.R
import unknow.chopchep.base.BaseActivity


class StoreInfoActivity : BaseActivity() {

    override var layoutRes = R.layout.activity_store_info

    private lateinit var frgProduct: ProductFragment
    private lateinit var storeFragment: StoreFragment
    private lateinit var transaction: FragmentTransaction

    override fun configUI() {
        super.configUI()
        frgProduct = ProductFragment()
        storeFragment = StoreFragment()
        repalaceFragment(storeFragment, "store")
    }

    override fun eventHandlers() {
        super.eventHandlers()

        btn_product.setOnClickListener {
            changeUI(btn_product)
            repalaceFragment(frgProduct, "product")
        }

        btn_store.setOnClickListener {
            changeUI(btn_store)
            repalaceFragment(storeFragment, "store")
        }

        ic_back.setOnClickListener {
            finish()
        }
    }

    private fun changeUI(btn: Button){
        btn.setTextColor(Color.WHITE)
        btn.setBackgroundResource(R.drawable.button_store_select)

        if (btn === btn_store){
            btn_product.setTextColor(Color.parseColor("#555550"))
            btn_product.setBackgroundResource(R.drawable.button_store_default)
            return
        }

        btn_store.setTextColor(Color.parseColor("#555550"))
        btn_store.setBackgroundResource(R.drawable.button_store_default)
    }

    private fun repalaceFragment(fragment: Fragment, tag: String) {
        val support = supportFragmentManager
        transaction = support.beginTransaction()
        transaction.replace(R.id.storeContainer, fragment).addToBackStack(tag).commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        frgProduct.onActivityResult(requestCode,resultCode,data)
    }
}
