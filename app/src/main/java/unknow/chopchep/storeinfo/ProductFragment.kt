package unknow.chopchep.storeinfo

import android.content.Intent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.product_fragment.*
import kotlinx.android.synthetic.main.rcv_product_row.view.*
import org.jetbrains.anko.startActivityForResult
import unknow.chopchep.R
import unknow.chopchep.base.BaseFragment
import unknow.chopchep.productdetail.ProductDetailActivity
import unknow.common.apiconfig.APIManager
import unknow.common.apiconfig.Api.AppExecutors
import unknow.common.customcontrol.init
import unknow.common.customcontrol.reloadData
import unknow.common.extension.jsonString
import unknow.common.extension.loadCircleImage
import unknow.common.extension.toDecimalString
import unknow.common.extension.toObject
import unknow.repository.remote.dto.BaseModel
import unknow.repository.remote.dto.ProductDTO

class ProductFragment : BaseFragment() {

    private val vm by lazy {
        activity?.let {
            ViewModelProviders.of(it).get(StoreInfoViewModel::class.java)
        }
    }

    private val deleteCode = 9
    private val updateCode = 99
    private var lsData = mutableListOf<ProductDTO>()

    override var layoutRes: Int = R.layout.product_fragment

    override fun getData() {
        super.getData()

        vm?.getAllItemsOfStore(AppExecutors(), BaseModel().apply {
            id = vm?.store?.id
        })?.observe(this, Observer {
            it.handle({
                lsData = it.data ?: mutableListOf()
                rcv_product.init(
                    lsData,
                    R.layout.rcv_product_row,
                    GridLayoutManager(this.activity, 2)
                ) { view, data ->
                    view.itemView.apply {
                        this.tv_product_name.text = data.name ?: ""
                        this.tv_product_price.text = data.price?.toDecimalString(0)
                        imv_product.loadCircleImage(APIManager.imgBaseURL + data.imageurl, R.drawable.ic_product_default)

                        setOnClickListener {
                            activity?.startActivityForResult<ProductDetailActivity>(updateCode,
                                "Product" to data.jsonString,
                                "Store" to vm?.store?.jsonString)
                        }
                    }
                }
            })
        })
    }

    override fun eventHandlers() {
        super.eventHandlers()

        btn_add.setOnClickListener {
            activity?.startActivityForResult<ProductDetailActivity>(updateCode, "Store" to vm?.store?.jsonString)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == deleteCode){
            data?.extras?.getInt("ProductID")?.let { id ->
                val product = lsData.first { it.id == id }
                lsData.remove(product)
                rcv_product.reloadData(lsData)
            }
            return
        }

        if (requestCode == updateCode){
            data?.extras?.getString("Product")?.toObject<ProductDTO>()?.let { pd ->
                val product = lsData.firstOrNull { it.id == pd.id }
                product?.let {
                    lsData[lsData.indexOf(it)] = pd
                    rcv_product.reloadData(lsData)
                    return
                }

                lsData.add(pd)
                rcv_product.reloadData(lsData)
            }
        }
    }
}