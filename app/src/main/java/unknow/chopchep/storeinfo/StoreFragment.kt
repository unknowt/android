package unknow.chopchep.storeinfo

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.store_fragment.*
import unknow.chopchep.R
import unknow.chopchep.base.Base
import unknow.chopchep.base.BaseFragment
import unknow.chopchep.userinfo.ImageHelper
import unknow.common.apiconfig.Api.AppExecutors
import unknow.common.extension.loadImage
import unknow.common.extension.setBorderWithRoudedCorner
import unknow.common.extension.showDialogChooseImage
import unknow.repository.remote.dto.CreateUpdateStoreRequest
import unknow.repository.remote.dto.GetStoreRequest
import unknow.repository.remote.dto.UpImageRequest
import java.io.File
import java.io.IOException
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.util.*

class StoreFragment : BaseFragment() {

    private val pickImageResult = 100
    private val takePhoToResult = 200

    var currentPhotoPath: String? = null

    private var uidImg = ""
    private val imageHelper: ImageHelper? by lazy {
        this.context?.let {  ImageHelper(WeakReference(it)) }
    }

    override var layoutRes = R.layout.store_fragment

    private val vm by lazy {
        activity?.let {
            ViewModelProviders.of(it).get(StoreInfoViewModel::class.java)
        }
    }

    override fun configUI() {
        super.configUI()
        edt_store_name.setBorderWithRoudedCorner(10f,1,Color.parseColor("#CACACA"))
    }

    override fun getData() {
        super.getData()

        vm?.getInfo(AppExecutors(), GetStoreRequest().apply {
            userid = Base.userDTO?.id
        })?.observe(this, Observer {
            it.handle({
                vm?.store = it.data
                imv_store.loadImage(it.data?.imageurl, R.drawable.ic_store_default)
                sw_status.isChecked = it.data?.status == 1
                edt_store_name.setText(it.data?.name)
            })
        })
    }

    override fun eventHandlers() {
        super.eventHandlers()

        imv_store.setOnClickListener {
            activity?.let {
                activity?.showDialogChooseImage({
                    if (ActivityCompat.checkSelfPermission(it, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        openCamera()
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(arrayOf(Manifest.permission.CAMERA), 9999)
                        }
                    }
                }, {
                    choseImageFromGalery()
                })
            }
        }

        btn_save.setOnClickListener {
            val request = CreateUpdateStoreRequest().apply {
                userID = Base.userDTO?.id
                name = edt_store_name.text.toString()
                status = if (sw_status.isChecked) 1 else 0
                id = vm?.store?.id
                imageurl = uidImg
            }
            vm?.updateInfo(AppExecutors(),request)?.observe(this, Observer {
                it.handle({
                    edt_store_name.clearFocus()
                })
            })
        }
    }

    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            activity?.let { act ->
                takePictureIntent.resolveActivity(act.packageManager)?.also {
                    var photoFile: File?
                    try {
                        photoFile = createImageFile()
                    } catch (ex: IOException) {
                        photoFile = null
                    }

                    photoFile?.also {
                        val photoURI: Uri = FileProvider.getUriForFile(act, "com.example.android.fileprovider", it)
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, takePhoToResult)
                    }
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 9999 && permissions.contains(Manifest.permission.CAMERA)){
            openCamera()
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir = activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            currentPhotoPath = absolutePath
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == pickImageResult) {
            data?.let {
                val resultBitmap = imageHelper?.resizeBitmap(data.data)
                imv_store.loadImage(resultBitmap, R.drawable.ic_user_default)
                uploadImage(resultBitmap, "image.jpg")
            }
        }

        if (requestCode == takePhoToResult) {
            val uri = Uri.fromFile(File(currentPhotoPath))
            val img = imageHelper?.resizeBitmap(uri)
            imv_store.loadImage(img, R.drawable.ic_user_default)

            val currentFile = File(currentPhotoPath)
            uploadImage(img, currentFile.name)
            if (currentFile.exists()) {
                currentFile.delete()
            }
        }
    }

    private fun choseImageFromGalery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Chọn hình từ thư viện"), pickImageResult)
    }

    private fun uploadImage(bitmap: Bitmap?, fileName: String){
        val request = UpImageRequest().apply {
            base64image = imageHelper?.bitmapToBase64String(bitmap)
            filename = fileName
        }

        vm?.upImage(AppExecutors(), request)?.observe(this, Observer {
            it.handle({
                uidImg = it.data?.uid ?: ""
            })
        })
    }
}
