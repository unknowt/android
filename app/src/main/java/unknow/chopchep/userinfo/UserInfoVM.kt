package unknow.chopchep.userinfo

import androidx.lifecycle.LiveData
import unknow.chopchep.base.BaseViewModel
import unknow.common.apiconfig.Api.ApiResponse
import unknow.common.apiconfig.Api.AppExecutors
import unknow.common.apiconfig.Api.NetworkBoundResource
import unknow.common.apiconfig.Api.Resource
import unknow.repository.remote.api.IUser
import unknow.repository.remote.dto.BaseModel
import unknow.repository.remote.dto.BaseResponse
import unknow.repository.remote.dto.ChangeInfoRequest

class UserInfoVM : BaseViewModel() {

    fun updateProfile(input: ChangeInfoRequest, appExecutors: AppExecutors): LiveData<Resource<BaseResponse<BaseModel>>> {
        return object : NetworkBoundResource<BaseResponse<BaseModel>, BaseResponse<BaseModel>>(appExecutors) {
            override fun saveCallResult(item: BaseResponse<BaseModel>) {

            }

            override fun shouldFetch(data: BaseResponse<BaseModel>?): Boolean {
                return true
            }

            override fun shouldSave(): Boolean {
                return false
            }

            override fun loadFromDb(): LiveData<BaseResponse<BaseModel>>? {
                return null
            }

            override fun createCall(): LiveData<ApiResponse<BaseResponse<BaseModel>>> {
                return api<IUser>()?.changeInfo(input)!!
            }
        }.asLiveData()
    }
}