package unknow.chopchep.userinfo

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_user_info.*
import unknow.chopchep.base.Base
import unknow.chopchep.base.BaseActivity
import unknow.common.apiconfig.APIManager
import unknow.common.extension.loadCircleImage
import unknow.common.extension.setBorderWithRoudedCorner
import unknow.common.extension.showDialogChooseImage
import unknow.repository.remote.dto.ChangeInfoRequest
import unknow.repository.remote.dto.UpImageRequest
import java.io.File
import java.io.IOException
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.util.*

class UserInfoActivity : BaseActivity() {

    private val pickImageResult = 100
    private val takePhoToResult = 200

    var currentPhotoPath: String? = null
    var uidImg = ""

    override var layoutRes = unknow.chopchep.R.layout.activity_user_info

    private val vm by lazy {
        ViewModelProviders.of(this).get(UserInfoVM::class.java)
    }

    private val imageHelper: ImageHelper by lazy {
        ImageHelper(WeakReference(this))
    }

    override fun configUI() {
        super.configUI()

        edt_name.setBorderWithRoudedCorner(10f, 1, Color.parseColor("#CACACA"))
        tv_phone.setBorderWithRoudedCorner(10f, 1, Color.parseColor("#CACACA"))

        edt_name.setText(Base.userDTO?.fullname ?: "")
        tv_phone.text = Base.userDTO?.username ?: ""
        imv_avatar.loadCircleImage(APIManager.imgBaseURL + Base.userDTO?.avatar, unknow.chopchep.R.drawable.ic_user_default)
    }

    override fun eventHandlers() {
        super.eventHandlers()

        btn_back.setOnClickListener {
            finish()
        }

        btn_save.setOnClickListener {
            updateProfile()
        }

        imv_avatar.setOnClickListener {
            showDialogChooseImage({
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    openCamera()
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(arrayOf(Manifest.permission.CAMERA), 9999)
                    }
                }
            }, {
                choseImageFromGalery()
            })
        }
    }

    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                var photoFile: File?
                try {
                    photoFile = createImageFile()
                } catch (ex: IOException) {
                    photoFile = null
                }
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(this, "com.example.android.fileprovider", it)
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, takePhoToResult)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 9999 && permissions.contains(Manifest.permission.CAMERA)){
            openCamera()
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            currentPhotoPath = absolutePath
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == pickImageResult) {
            data?.data?.let {
                val resultBitmap = imageHelper.resizeBitmap(it)
                imv_avatar.loadCircleImage(resultBitmap, unknow.chopchep.R.drawable.ic_user_default)
                uploadImage(resultBitmap, "image.jpg")

            }
        }

        if (requestCode == takePhoToResult) {
            val uri = Uri.fromFile(File(currentPhotoPath))
            val img = imageHelper.resizeBitmap(uri)
            imv_avatar.loadCircleImage(img, unknow.chopchep.R.drawable.ic_user_default)

            val currentFile = File(currentPhotoPath)
            uploadImage(img, currentFile.name)
            if (currentFile.exists()) {
                currentFile.delete()
            }
        }
    }

    private fun uploadImage(bitmap: Bitmap?, fileName: String){
        val request = UpImageRequest().apply {
            base64image = imageHelper.bitmapToBase64String(bitmap)
            filename = fileName
        }

        vm.upImage(appExecutors, request).observe(this, Observer {
            it.handle({
                uidImg = it.data?.uid ?: ""
            })
        })
    }

    private fun choseImageFromGalery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Chọn hình từ thư viện"), pickImageResult)
    }

    private fun updateProfile() {
        val userDTO = Base.userDTO

        val request = ChangeInfoRequest().apply {
            id = userDTO?.id
            fullName = edt_name.text?.toString() ?: ""
            avatar = uidImg
        }

        vm.updateProfile(request, appExecutors).observe(this, Observer {
            it.handle({
                Base.userDTO = userDTO?.apply {
                    fullname = edt_name?.text?.toString() ?: ""
                    avatar = request.avatar
                }
                finish()
            })
        })
    }
}
