package unknow.chopchep.login

import androidx.lifecycle.LiveData
import unknow.chopchep.base.BaseViewModel
import unknow.common.apiconfig.APIManager
import unknow.common.apiconfig.Api.ApiResponse
import unknow.common.apiconfig.Api.AppExecutors
import unknow.common.apiconfig.Api.NetworkBoundResource
import unknow.common.apiconfig.Api.Resource
import unknow.repository.remote.api.IUser
import unknow.repository.remote.dto.BaseResponse
import unknow.repository.remote.dto.LoginRegisterRequest
import unknow.repository.remote.dto.UserDTO

class LoginViewModel : BaseViewModel() {
    val api by lazy {
        APIManager.client?.create(IUser::class.java)
    }

    fun login(appExecutors: AppExecutors, input: LoginRegisterRequest): LiveData<Resource<BaseResponse<UserDTO>>>? {

        return object : NetworkBoundResource<BaseResponse<UserDTO>, BaseResponse<UserDTO>>(appExecutors) {
            override fun saveCallResult(item: BaseResponse<UserDTO>) {

            }

            override fun shouldFetch(data: BaseResponse<UserDTO>?): Boolean {
                return true
            }

            override fun shouldSave(): Boolean {
                return false
            }

            override fun loadFromDb(): LiveData<BaseResponse<UserDTO>>? {
                return null
            }

            override fun createCall(): LiveData<ApiResponse<BaseResponse<UserDTO>>> {
                return api?.login(input)!!
            }

        }.asLiveData()
    }
}