package unknow.chopchep.login


import android.content.Intent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.accountkit.*
import com.facebook.accountkit.ui.AccountKitActivity
import com.facebook.accountkit.ui.AccountKitConfiguration
import com.facebook.accountkit.ui.LoginType
import org.jetbrains.anko.startActivity
import unknow.chopchep.R
import unknow.chopchep.base.Base
import unknow.chopchep.base.BaseActivity
import unknow.chopchep.home.HomeActivity
import unknow.repository.remote.dto.LoginRegisterRequest
import java.util.*

class LoginActivity : BaseActivity() {

    private var REQUESTCODE = 99
    private val vm: LoginViewModel by lazy {
        ViewModelProviders.of(this).get(LoginViewModel::class.java)
    }
    override var layoutRes = R.layout.activity_login

    override fun configUI() {
        super.configUI()
        openAccountKit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity<HomeActivity>()
    }

    private fun openAccountKit() {
        val res = this.resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.setLocale(Locale("vi_VN"))
        res.updateConfiguration(conf, dm)

        val intent = Intent(this, AccountKitActivity::class.java)
        val configurationBuilder = AccountKitConfiguration.AccountKitConfigurationBuilder(
            LoginType.PHONE,
            AccountKitActivity.ResponseType.TOKEN
        )
        configurationBuilder.setDefaultCountryCode("vi_VN")
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, configurationBuilder.build())
        startActivityForResult(intent, REQUESTCODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUESTCODE) {
            val loginResult = data?.getParcelableExtra<AccountKitLoginResult>(AccountKitLoginResult.RESULT_KEY)
            loginResult?.error?.let {
                showDiaLogError(loginResult.error?.errorType?.message ?: "")
                return
            }
            if (loginResult?.wasCancelled() == true) {
                startActivity<HomeActivity>()
                return
            }

            loginResult?.accessToken?.let {
                loginResult.accessToken?.accountId
            }

            AccountKit.getCurrentAccount(
                object : AccountKitCallback<Account> {
                    override fun onSuccess(p0: Account?) {
                        val mobile = p0?.phoneNumber?.phoneNumber ?: ""
                        val inputLogin = LoginRegisterRequest()
                        inputLogin.userName = mobile
                        loginRegister(inputLogin)
                    }

                    override fun onError(p0: AccountKitError?) {
                        showDiaLogError(p0?.errorType?.message ?: "")
                    }
                })
        }
    }

    fun loginRegister(inputLogin: LoginRegisterRequest) {
        vm.login(appExecutors, inputLogin)?.observe(this, Observer {
            it.handle({
                Base.userDTO = it.data
                startActivity<HomeActivity>()
            })
        })
    }
}
