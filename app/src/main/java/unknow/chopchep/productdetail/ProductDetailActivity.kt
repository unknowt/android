package unknow.chopchep.productdetail

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_product_detail.*
import unknow.chopchep.R
import unknow.chopchep.base.Base
import unknow.chopchep.base.BaseActivity
import unknow.chopchep.userinfo.ImageHelper
import unknow.common.apiconfig.APIManager
import unknow.common.apiconfig.Api.AppExecutors
import unknow.common.extension.*
import unknow.repository.remote.dto.BaseModel
import unknow.repository.remote.dto.ProductDTO
import unknow.repository.remote.dto.StoreDTO
import unknow.repository.remote.dto.UpImageRequest
import java.io.File
import java.io.IOException
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.util.*

class ProductDetailActivity : BaseActivity(){

    private val pickImageResult = 100
    private val takePhoToResult = 200

    var currentPhotoPath: String? = null
    private  var uidImg = ""

    private val imageHelper: ImageHelper by lazy {
        ImageHelper(WeakReference(this))
    }

    override var layoutRes = R.layout.activity_product_detail

    private val vm by lazy {
        ViewModelProviders.of(this).get(ProductDetailViewModel::class.java)
    }

    private val product by lazy {
        intent?.extras?.getString("Product")?.toObject<ProductDTO>()
    }

    private val store by lazy {
        intent?.extras?.getString("Store")?.toObject<StoreDTO>()
    }

    override fun configUI() {
        super.configUI()

        imv_product.loadImage(APIManager.imgBaseURL + product?.imageurl, R.drawable.ic_product_default)
        edt_name.setText(product?.name)
        edt_price.setText(product?.price?.toDecimalString(0))
        sw_active.isChecked = product?.status == 1
        edt_price.setBorderWithRoudedCorner(5f,1, Color.parseColor("#555550"))
        edt_name.setBorderWithRoudedCorner(5f,1, Color.parseColor("#555550"))

        btn_delete.gone
        product?.let {
            btn_delete.visiable
        }
    }

    override fun eventHandlers() {
        super.eventHandlers()

        btn_back.setOnClickListener {
            finish()
        }

        btn_delete.setOnClickListener{
            vm.delete(appExecutors, BaseModel().apply {
                id = product?.id
            }).observe(this, Observer {
                it.handle({
                    setResult(9, intent.putExtra("ProductID", product?.id))
                    finish()
                })
            })
        }

        btn_save.setOnClickListener{
            product?.let {
                updateProduct(it)
                return@setOnClickListener
            }

            createProduct()
        }

        imv_product.setOnClickListener {
            showDialogChooseImage({
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    openCamera()
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(arrayOf(Manifest.permission.CAMERA), 9999)
                    }
                }
            }, {
                choseImageFromGalery()
            })
        }
    }

    private fun updateProduct(productDTO: ProductDTO){
        val request = productDTO.apply {
            id = product?.id
            name = edt_name.text.toString()
            price = edt_price.text.toString().toDouble()
            storeid = store?.id
            status = if (sw_active.isChecked) 1 else 0
            usercreated = Base.userDTO?.id
            imageurl = uidImg
        }

        vm.update(appExecutors, request).observe(this, Observer {
            it.handle({
                setResult(99, intent.putExtra("Product", request.jsonString))
                finish()
            })
        })
    }

    private fun createProduct(){
        val request = ProductDTO().apply {
            name = edt_name.text.toString()
            price = edt_price.text.toString().toDouble()
            storeid = store?.id
            status = if (sw_active.isChecked) 1 else 0
            usercreated = Base.userDTO?.id
            imageurl = uidImg
        }

        vm.create(appExecutors, request).observe(this, Observer {
            it.handle({
                setResult(99, intent.putExtra("Product", request.apply { id = it.data?.id }.jsonString))
                finish()
            })
        })
    }


    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                var photoFile: File?
                try {
                    photoFile = createImageFile()
                } catch (ex: IOException) {
                    photoFile = null
                }
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(this, "com.example.android.fileprovider", it)
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, takePhoToResult)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 9999 && permissions.contains(Manifest.permission.CAMERA)){
            openCamera()
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            currentPhotoPath = absolutePath
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == pickImageResult) {
            data?.let {
                val resultBitmap = imageHelper.resizeBitmap(data.data)
                imv_product.loadImage(resultBitmap, R.drawable.ic_product_default)
                uploadImage(resultBitmap, "image.jpg")
            }
        }

        if (requestCode == takePhoToResult) {
            val uri = Uri.fromFile(File(currentPhotoPath))
            val img = imageHelper.resizeBitmap(uri)
            imv_product.loadImage(img, R.drawable.ic_product_default)

            val currentFile = File(currentPhotoPath)
            uploadImage(img, currentFile.name)
            if (currentFile.exists()) {
                currentFile.delete()
            }
        }
    }

    private fun choseImageFromGalery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Chọn hình từ thư viện"), pickImageResult)
    }

    private fun uploadImage(bitmap: Bitmap?, fileName: String){
        val request = UpImageRequest().apply {
            base64image = imageHelper?.bitmapToBase64String(bitmap)
            filename = fileName
        }

        vm.upImage(AppExecutors(), request).observe(this, Observer {
            it.handle({
                uidImg = it.data?.uid ?: ""
            })
        })
    }
}
