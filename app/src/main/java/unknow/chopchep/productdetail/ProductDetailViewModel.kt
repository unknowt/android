package unknow.chopchep.productdetail

import androidx.lifecycle.LiveData
import unknow.chopchep.base.BaseViewModel
import unknow.common.apiconfig.APIManager
import unknow.common.apiconfig.Api.ApiResponse
import unknow.common.apiconfig.Api.AppExecutors
import unknow.common.apiconfig.Api.NetworkBoundResource
import unknow.common.apiconfig.Api.Resource
import unknow.repository.remote.api.IProduct
import unknow.repository.remote.dto.BaseModel
import unknow.repository.remote.dto.BaseResponse
import unknow.repository.remote.dto.ProductDTO

class  ProductDetailViewModel: BaseViewModel(){

    private val api by lazy {
        APIManager.client?.create(IProduct::class.java)
    }

    fun update(
        appExecutors: AppExecutors,
        productDTO: ProductDTO): LiveData<Resource<BaseResponse<String>>> {
        return object : NetworkBoundResource<BaseResponse<String>, BaseResponse<String>>(appExecutors){
            override fun saveCallResult(item: BaseResponse<String>) {

            }

            override fun shouldFetch(data: BaseResponse<String>?): Boolean {
                return true
            }

            override fun shouldSave(): Boolean {
                return false
            }
            override fun loadFromDb(): LiveData<BaseResponse<String>>? {
                return null
            }

            override fun createCall(): LiveData<ApiResponse<BaseResponse<String>>> {
                return api?.update(productDTO)!!
            }
        }.asLiveData()
    }

    fun create(
        appExecutors: AppExecutors,
        productDTO: ProductDTO): LiveData<Resource<BaseResponse<BaseModel>>> {
        return object : NetworkBoundResource<BaseResponse<BaseModel>, BaseResponse<BaseModel>>(appExecutors){
            override fun saveCallResult(item: BaseResponse<BaseModel>) {

            }

            override fun shouldFetch(data: BaseResponse<BaseModel>?): Boolean {
                return true
            }

            override fun shouldSave(): Boolean {
                return false
            }
            override fun loadFromDb(): LiveData<BaseResponse<BaseModel>>? {
                return null
            }

            override fun createCall(): LiveData<ApiResponse<BaseResponse<BaseModel>>> {
                return api?.create(productDTO)!!
            }
        }.asLiveData()
    }

    fun delete(
        appExecutors: AppExecutors,
        model: BaseModel): LiveData<Resource<BaseResponse<String>>> {
        return object : NetworkBoundResource<BaseResponse<String>, BaseResponse<String>>(appExecutors){
            override fun saveCallResult(item: BaseResponse<String>) {

            }

            override fun shouldFetch(data: BaseResponse<String>?): Boolean {
                return true
            }

            override fun shouldSave(): Boolean {
                return false
            }
            override fun loadFromDb(): LiveData<BaseResponse<String>>? {
                return null
            }

            override fun createCall(): LiveData<ApiResponse<BaseResponse<String>>> {
                return api?.delete(model)!!
            }
        }.asLiveData()
    }
}