package unknow.chopchep.product

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_product.*
import kotlinx.android.synthetic.main.rcv_product_row.view.*
import unknow.chopchep.R
import unknow.chopchep.base.BaseActivity
import unknow.common.apiconfig.APIManager
import unknow.common.customcontrol.init
import unknow.common.extension.loadCircleImage
import unknow.common.extension.toDecimalString
import unknow.common.extension.toObject
import unknow.repository.remote.dto.BaseModel
import unknow.repository.remote.dto.StoreDTO


class ProductActivity : BaseActivity() {

    override var layoutRes = unknow.chopchep.R.layout.activity_product

    private val vm by lazy {
        ViewModelProviders.of(this).get(ProductViewModel::class.java)
    }

    private val store by lazy {
        intent.getStringExtra(getString(unknow.chopchep.R.string.store)).toObject<StoreDTO>()
    }

    override fun configUI() {
        super.configUI()
        tv_title.text = store?.name
    }

    override fun getData() {
        super.getData()
        vm.getProductActive(appExecutors, BaseModel().apply {
            id = store?.id
        }).observe(this, Observer {
            it.handle({
                rcv_product.init(
                    it.data ?: mutableListOf(),
                    R.layout.rcv_product_row,
                    GridLayoutManager(this, 2))
                { view, data ->
                    view.itemView.apply {
                        tv_product_name.text = data.name
                        tv_product_price.text = data.price?.toDecimalString(0)
                        imv_product.loadCircleImage(APIManager.imgBaseURL + data.imageurl, R.drawable.ic_product_default)
                    }
                }
            })
        })
    }

    override fun eventHandlers() {
        super.eventHandlers()
        btn_back.setOnClickListener {
            finish()
        }

        btn_phone.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                call()
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), 9999)
                }
            }
        }
    }

    private fun call(){
        vm.logHistory(store?.id)
        val callIntent = Intent(Intent.ACTION_CALL)
        callIntent.data = Uri.parse("tel:${store?.contact ?: "0366984883"}")
        this.startActivity(callIntent)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 9999 && permissions.contains(Manifest.permission.CALL_PHONE)){
            call()
        }
    }
}
