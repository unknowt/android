package unknow.chopchep.product

import androidx.lifecycle.LiveData
import unknow.chopchep.base.BaseViewModel
import unknow.common.apiconfig.APIManager
import unknow.common.apiconfig.Api.ApiResponse
import unknow.common.apiconfig.Api.AppExecutors
import unknow.common.apiconfig.Api.NetworkBoundResource
import unknow.common.apiconfig.Api.Resource
import unknow.repository.remote.api.IProduct
import unknow.repository.remote.api.IStore
import unknow.repository.remote.dto.BaseModel
import unknow.repository.remote.dto.BaseResponse
import unknow.repository.remote.dto.LogHistoryRequest
import unknow.repository.remote.dto.ProductDTO

class ProductViewModel : BaseViewModel() {

    val api by lazy {
        APIManager.client?.create(IProduct::class.java)
    }

    val apiStore by lazy {
        APIManager.client?.create(IStore::class.java)
    }

    fun getProductActive(
        appExecutors: AppExecutors,
        baseModel: BaseModel
    ): LiveData<Resource<BaseResponse<MutableList<ProductDTO>>>> {
        return object :
            NetworkBoundResource<BaseResponse<MutableList<ProductDTO>>, BaseResponse<MutableList<ProductDTO>>>(
                appExecutors
            ) {
            override fun saveCallResult(item: BaseResponse<MutableList<ProductDTO>>) {

            }

            override fun shouldFetch(data: BaseResponse<MutableList<ProductDTO>>?): Boolean {
                return true
            }

            override fun shouldSave(): Boolean {
                return false
            }

            override fun loadFromDb(): LiveData<BaseResponse<MutableList<ProductDTO>>>? {
                return null
            }

            override fun createCall(): LiveData<ApiResponse<BaseResponse<MutableList<ProductDTO>>>> {
                return api<IProduct>()?.getActive(baseModel)!!
            }

        }.asLiveData()
    }

    fun logHistory(storeID: Int?){
        apiStore?.logHistory(LogHistoryRequest().apply {
            storeid = storeID
        })
    }
}