package unknow.chopchep.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import unknow.common.apiconfig.APIManager
import unknow.common.apiconfig.Api.ApiResponse
import unknow.common.apiconfig.Api.AppExecutors
import unknow.common.apiconfig.Api.NetworkBoundResource
import unknow.common.apiconfig.Api.Resource
import unknow.repository.remote.api.IFile
import unknow.repository.remote.dto.BaseResponse
import unknow.repository.remote.dto.UpImageRequest
import unknow.repository.remote.dto.UpImageResponse

open class BaseViewModel : ViewModel() {

    val error = MutableLiveData<String>()
    val isShowLoading = MutableLiveData<Boolean>()

    val apiUpFile by lazy {
        APIManager.client?.create(IFile::class.java)
    }

    inline fun <reified T> api(): T? {
        return APIManager.client?.create(T::class.java)
    }

    fun upImage(appExecutors: AppExecutors, request: UpImageRequest): LiveData<Resource<BaseResponse<UpImageResponse>>> {
        return object : NetworkBoundResource<BaseResponse<UpImageResponse>, BaseResponse<UpImageResponse>>(appExecutors){
            override fun saveCallResult(item: BaseResponse<UpImageResponse>) {

            }

            override fun shouldFetch(data: BaseResponse<UpImageResponse>?): Boolean {
                return true
            }

            override fun shouldSave(): Boolean {
                return false
            }
            override fun loadFromDb(): LiveData<BaseResponse<UpImageResponse>>? {
                return null
            }

            override fun createCall(): LiveData<ApiResponse<BaseResponse<UpImageResponse>>> {
                return apiUpFile?.upImage(request)!!
            }
        }.asLiveData()
    }
}
