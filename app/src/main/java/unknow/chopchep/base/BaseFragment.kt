package unknow.chopchep.base

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import unknow.common.apiconfig.Api.Resource
import unknow.common.apiconfig.Api.Status
import unknow.repository.remote.dto.BaseResponse

abstract class  BaseFragment : Fragment() {

    abstract var layoutRes: Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configUI()
        observerData()
        getData()
        eventHandlers()
    }

    open fun getData() {}

    open fun configUI() {}

    open fun eventHandlers() {}

    open fun observerData() {}

    fun <T: BaseResponse<*>> Resource<T>.handle(data: (T) -> Unit, execute: (() -> Unit)?= null) {
        val defaultMess = "Mạng không ổn định"
        when (this.status) {
            Status.LOADING -> {
                showLoading()
            }

            Status.SUCCESS -> {
                hideLoading()
                this.data?.let { response ->
                    if (response.status != true) {
                        showDiaLogError(response.message ?: defaultMess)
                        // invoke retry event
                        return
                    }

                    data.invoke(response)
                    return
                }

                showDiaLogError(defaultMess)
                // invoke retry event
            }

            Status.NULL, Status.ERROR -> {
                hideLoading()
                showDiaLogError(defaultMess)
                // invoke retry event
            }
        }
    }

    fun hideLoading() {
//        activity?.contentView?.hideLoading()
    }

    fun showLoading() {
//        activity?.contentView?.showLoading()
    }

    fun showDiaLogError(message: String) {
        val alertDialog = AlertDialog.Builder(activity)
        alertDialog.setTitle("Cảnh Báo")
        alertDialog.setMessage(message)
        alertDialog.setPositiveButton(android.R.string.yes) { dialog, which ->
            dialog.dismiss()
        }
        val dialog = alertDialog.create()
        dialog.show()
    }
}