package unknow.chopchep.base

import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import unknow.chopchep.R
import unknow.common.extension.get
import unknow.common.extension.jsonString
import unknow.common.extension.put
import unknow.common.extension.toObject
import unknow.repository.remote.dto.UserDTO

object Base {

    var sharedPreferences: SharedPreferences? = null

    var userDTO: UserDTO? = null
        get() {
            val userJson = sharedPreferences?.get<String>("UserLocal")
            return userJson?.toObject()
        }
        set(value) {
            sharedPreferences?.put("UserLocal", (value ?: "").jsonString)
            field = value
        }
}

fun View?.showLoading() {
    this?.context?.let {
        hideLoading()
        val view = LayoutInflater.from(it).inflate(R.layout.loading, null)
        view.elevation = 99f
        view.layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT)
        view.tag = "Loading"
        view.setOnClickListener {  }
        (this as? ViewGroup)?.addView(view)
    }
}

fun View?.hideLoading() {
    val loadingView = this?.findViewWithTag<ViewGroup>("Loading") ?: return
    (this as? ViewGroup)?.removeView(loadingView)
}