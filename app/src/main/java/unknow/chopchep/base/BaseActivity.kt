package unknow.chopchep.base

import android.app.AlertDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import unknow.common.apiconfig.Api.AppExecutors
import unknow.common.apiconfig.Api.Resource
import unknow.common.apiconfig.Api.Status
import unknow.repository.remote.dto.BaseResponse

abstract class BaseActivity : AppCompatActivity() {

    abstract var layoutRes: Int

    val appExecutors by lazy {
        AppExecutors()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)
        getData()
        configUI()
        eventHandlers()
        observerData()
    }

    open fun configUI() {}

    open fun getData() {}

    open fun eventHandlers() {}

    open fun observerData() {}

    fun <T: BaseResponse<*>> Resource<T>.handle(data: (T) -> Unit, execute: (() -> Unit)?= null) {
        val defaultMess = "Mạng không ổn định"
        when (this.status) {
            Status.LOADING -> {
                showLoading()
            }

            Status.SUCCESS -> {
                hideLoading()
                this.data?.let { response ->
                    if (response.status != true) {
                        showDiaLogError(response.message ?: defaultMess)
                        // invoke retry event
                        return
                    }

                    data.invoke(response)
                    return
                }

                showDiaLogError(defaultMess)
                // invoke retry event
            }

            Status.NULL, Status.ERROR -> {
                hideLoading()
                showDiaLogError(defaultMess)
                // invoke retry event
            }
        }
    }

    fun hideLoading() {
//        contentView.hideLoading()
    }

    fun showLoading() {
//        contentView.showLoading()
    }

    fun showDiaLogError(message: String) {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle("Cảnh Báo")
        alertDialog.setMessage(message)
        alertDialog.setPositiveButton(android.R.string.yes) { dialog, which ->
            dialog.dismiss()
        }
        val dialog = alertDialog.create()
        dialog.show()
    }
}