package unknow.common.apiconfig

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import unknow.common.apiconfig.Api.LiveDataCallAdapterFactory
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

object APIManager {

    var imgBaseURL = ""
    var client: Retrofit? = null

    var url: String by Delegates.observable("") { _, _, newValue ->
        if (newValue.isNotEmpty()) {
            client = createRetrofit()
        }
    }

    private fun createRetrofit(): Retrofit? {
        val okHttpClient = OkHttpClient.Builder().apply {
            connectTimeout(1, TimeUnit.MINUTES)
            readTimeout(1, TimeUnit.MINUTES)
            writeTimeout(1, TimeUnit.MINUTES)
            addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            retryOnConnectionFailure(true)
        }.build()

        return Retrofit.Builder().apply {
            baseUrl(if (url.trimEnd() == "/") url else "$url/")
            client(okHttpClient)
            addCallAdapterFactory(LiveDataCallAdapterFactory())
            addConverterFactory(GsonConverterFactory.create(
                GsonBuilder()
                    .serializeNulls()
                    .create()
            ))
        }.build()
    }
}