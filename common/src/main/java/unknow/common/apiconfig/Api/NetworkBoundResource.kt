package unknow.common.apiconfig.Api

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

abstract class NetworkBoundResource<ResultType, RequestType> {
    private var appExecutors: AppExecutors

    private val result = MediatorLiveData<Resource<ResultType>>()

    @MainThread
    constructor(appExecutors: AppExecutors) {
        this.appExecutors = appExecutors
        result.value = Resource.loading(null)

        fetchFromNetwork()
    }

    private fun fetchFromNetwork() {
        val apiResponse = createCall()
        result.addSource(apiResponse) { response ->
            if (response is ApiResponse.ApiSuccessResponse) {
                if (response is ApiResponse.ApiEmptyResponse) {
                    result.setValue(Resource.responseNull())
                } else {
                    result.value = Resource.success((response.body as ResultType))
                    if (shouldSave()) {
                        appExecutors.diskIO().execute {
                            saveCallResult((response.body as RequestType))
                        }
                    }
                }
            } else {
                result.value = Resource.error((response as ApiResponse.ApiErrorResponse).errorMessage, null)
                onFetchFailed()
            }
        }
    }

    private fun onFetchFailed() {}

    fun asLiveData(): LiveData<Resource<ResultType>> {
        return result
    }

    @WorkerThread
    protected fun processResponse(response: ApiResponse<RequestType>): RequestType? {
        return (response as ApiResponse.ApiSuccessResponse).body
    }

    @WorkerThread
    protected abstract fun saveCallResult(item: RequestType)

    @MainThread
    protected abstract fun shouldFetch(data: ResultType?): Boolean

    @MainThread
    protected abstract fun shouldSave(): Boolean

    @MainThread
    protected abstract fun loadFromDb(): LiveData<ResultType>?

    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<RequestType>>
}