package unknow.common.apiconfig.Api


import androidx.lifecycle.LiveData
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type

class LiveDataCallAdapter<T>(val responseType: Type) : CallAdapter<T, LiveData<ApiResponse<T>>> {
    override fun adapt(call: Call<T>): LiveData<ApiResponse<T>> {
        return object : LiveData<ApiResponse<T>>() {
            var isSuccess = false
            override fun onActive() {
                super.onActive()
                if (!isSuccess && !call.isExecuted) enqueue()
            }

            override fun onInactive() {
                super.onInactive()
                dequeue()
            }

            private fun dequeue() {
                if (call.isExecuted) call.cancel()
            }

            private fun enqueue() {
                call.enqueue(object : Callback<T> {
                    override fun onFailure(call: Call<T>, t: Throwable) {
                        postValue(ApiResponse.create(-1, t))
                    }

                    override fun onResponse(call: Call<T>, response: Response<T>) {
                        postValue(ApiResponse.create(response))
                        isSuccess = true
                    }

                })
            }
        }
    }

    override fun responseType(): Type = responseType
}