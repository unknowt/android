package unknow.common.apiconfig.Api

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
    NULL;
}