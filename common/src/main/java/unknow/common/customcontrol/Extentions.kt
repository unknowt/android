package unknow.common.customcontrol

import android.app.Activity
import android.view.View

fun View.findLayout(layout: Int, activity: Activity): View {
    val layoutInflater = activity.layoutInflater
    return layoutInflater.inflate(layout, null, false)
}
