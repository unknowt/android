package unknow.common.customcontrol

import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import unknow.common.extension.inflate

class RecycleViewAdapter<T> : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var lsData = mutableListOf<T>()
    var bindData: ((RecyclerView.ViewHolder, T) -> Unit)? = null
    @LayoutRes
    var layoutResID: Int = 0
    override fun getItemCount() = lsData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return object : RecyclerView.ViewHolder(parent.inflate(layoutResID)) {}
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        this@RecycleViewAdapter.bindData?.invoke(holder, lsData[position])
    }
}

fun <T> RecyclerView.init(lsData: MutableList<T>, @LayoutRes layoutResID: Int, layoutManager: RecyclerView.LayoutManager? = null, bindData: ((RecyclerView.ViewHolder, T) -> Unit)) {
    this.layoutManager = layoutManager ?: LinearLayoutManager(this.context).apply {
        orientation = RecyclerView.VERTICAL
    }

    adapter = RecycleViewAdapter<T>().apply {
        this.layoutResID = layoutResID
        this.lsData = lsData
        this.bindData = bindData
    }
}

fun <T> RecyclerView.reloadData(lsData: MutableList<T>?) {
    val adapter = (this.adapter as? RecycleViewAdapter<T>)
    adapter?.lsData = lsData ?: mutableListOf()
    adapter?.notifyDataSetChanged()
}