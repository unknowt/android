package unknow.common.extension

import com.google.gson.Gson


val gson: Gson by lazy {
    Gson()
}

val Any.jsonString: String?
    get() {
        return gson.toJson(this)
    }

inline fun <reified T> Any.clone(): T? {
    return try {
        this.jsonString?.toObject()
    } catch (e: Exception) {
        null
    }
}
