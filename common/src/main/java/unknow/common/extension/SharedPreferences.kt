package unknow.common.extension

import android.content.SharedPreferences

fun <T> SharedPreferences.put(key: String, data: T) {
    edit().apply {
        when (data) {
            is String -> {
                putString(key, data)
            }

            is Boolean -> putBoolean(key, data)

            is Int -> putInt(key, data)

            is Float -> {
                putFloat(key, data)
            }
        }
    }.apply()
}

inline fun <reified T : Any> SharedPreferences.get(key: String): T? {
    return when (T::class) {
        String::class -> getString(key, null) as? T
        Int::class -> getInt(key, -1) as? T
        Boolean::class -> getBoolean(key, false) as? T
        Float::class -> getFloat(key, -1f) as? T
        else -> throw UnsupportedOperationException("Not yet implemented")
    }
}