package unknow.common.extension

import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


fun ImageView.loadCircleImage(any: Any?, @DrawableRes placeHolder: Int?) {
    if (any == null) {
        placeHolder?.let {
            setImageResource(it)
        }
        return
    }

    Glide.with(this)
        .load(any)
        .apply(RequestOptions.circleCropTransform())
        .apply(if (placeHolder != null) RequestOptions.placeholderOf(placeHolder) else RequestOptions.noTransformation())
        .into(this)
}

fun ImageView.loadImage(any: Any?, @DrawableRes placeHolder: Int?) {
    if (any == null) {
        placeHolder?.let {
            setImageResource(it)
        }
        return
    }

    Glide.with(this)
        .load(any)
        .apply(if (placeHolder != null) RequestOptions.placeholderOf(placeHolder) else RequestOptions.noTransformation())
        .into(this)
}