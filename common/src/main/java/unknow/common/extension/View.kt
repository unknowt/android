package unknow.common.extension

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.view.inputmethod.InputMethodManager

fun View.setBorder(thickness: Int = 1, color: Int = Color.BLACK) {
    val gradientDrawable = GradientDrawable()
    gradientDrawable.setStroke(thickness, color)
    gradientDrawable.setColor((background as? ColorDrawable)?.color
        ?: Color.TRANSPARENT)
    background = gradientDrawable
    setPadding(thickness,thickness,thickness,thickness)
}

fun View.setCornerRadius(radius: Float) {
    val gradientDrawable = GradientDrawable()
    gradientDrawable.cornerRadius = radius
    gradientDrawable.setColor((background as? ColorDrawable)?.color
        ?: Color.TRANSPARENT)
    background = gradientDrawable
}

fun View.setBorderWithRoudedCorner(radius: Float, thickness: Int = 1, color: Int = Color.BLACK) {
    val gradientDrawable = GradientDrawable()
    gradientDrawable.cornerRadius = radius
    gradientDrawable.setStroke(thickness, color)
    gradientDrawable.setColor((background as? ColorDrawable)?.color
        ?: Color.TRANSPARENT)
    background = gradientDrawable
}

fun View.setBorderWithRoudedCorner(radius: Float, thickness: Int = 1, color: Int = Color.BLACK, bgColor: Int) {
    val gradientDrawable = GradientDrawable()
    gradientDrawable.cornerRadius = radius
    gradientDrawable.setStroke(thickness, color)
    gradientDrawable.setColor(bgColor)
    background = gradientDrawable
}

val View.visiable: Unit
    get() {
        this.visibility = View.VISIBLE
    }

val View.isVisiable: Boolean
    get() {
        return this.visibility == View.VISIBLE
    }

val View.inVisiable: Unit
    get() {
        this.visibility = View.INVISIBLE
    }

val View.isInVisiable: Boolean
    get() {
        return this.visibility == View.INVISIBLE
    }

val View.gone: Unit
    get() {
        this.visibility = View.GONE
    }

val View.isGone: Boolean
    get() {
        return this.visibility == View.GONE
    }


fun View.hideKeyBoard() {
    (context.getSystemService(Context.INPUT_METHOD_SERVICE)
            as? InputMethodManager)?.hideSoftInputFromWindow(this.windowToken, 0)
}

fun View.showKeyBoard() {
    (context.getSystemService(Context.INPUT_METHOD_SERVICE)
            as? InputMethodManager)?.showSoftInput(this, 0)
}