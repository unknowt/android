package unknow.common.extension

import com.google.gson.reflect.TypeToken

inline fun <reified T> String.toObject(): T? {
    return try {
        gson.fromJson(this, T::class.java)
    } catch (e: Exception) {
        null
    }
}

inline fun <reified T> String.toObjects(): List<T>? {
    return try {
        gson.fromJson<List<T>>(this, object : TypeToken<List<T>>() {}.type)
    } catch (e: Exception) {
        null
    }
}