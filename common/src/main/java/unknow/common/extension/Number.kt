package unknow.common.extension

import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

fun Number.toDecimalString(decimal: Int = 2): String {
    val locale = Locale("en")
    val pattern = "#,###.##"
    val decimalFormat = NumberFormat.getNumberInstance(locale) as? DecimalFormat
    decimalFormat?.applyPattern(pattern)
    decimalFormat?.minimumFractionDigits = decimal
    decimalFormat?.maximumFractionDigits = decimal
    return decimalFormat?.format(this) ?: ""
}
