package unknow.common.extension

import android.app.Activity
import android.app.AlertDialog

fun Activity?.showDialogChooseImage(fromCamera:() -> Unit, fromGalery: () -> Unit){
    AlertDialog.Builder(this).apply {
        setTitle("Cập nhật ảnh đại diện")
        setMessage("Bạn muốn chọn ảnh từ ?")
        setPositiveButton("Chụp ảnh") { _, _ ->
            fromCamera()
        }
        setNegativeButton("Thư viện ảnh") { _, _ ->
            fromGalery()
        }
    }.create().show()
}